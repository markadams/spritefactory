#!/usr/bin/python -O

import Image
import os
import os.path
import re
import argparse


class SpriteComponent():
    metadata_regex = re.compile(
        '^(?:[0-9]+)_(?:([0-9]*)px_)?(?:([0-9]*)px_)?'
    )

    def __init__(self, image, filename):
        self.image = image
        box = self.image.getbbox()
        self.height = box[3]
        self.width = box[2]
        self.filename = os.path.splitext(os.path.basename(filename))[0]

    def classname(self):
        return SpriteComponent.metadata_regex.sub('', self.filename)

    def padding(self):
        regex = SpriteComponent.metadata_regex.match(self.filename)

        if regex is None:
            return (0, 0)

        vert, horiz = regex.groups()

        if horiz is None and vert is None:
            return (0, 0)

        if horiz is None:
            horiz = vert

        horiz = int(horiz)
        vert = int(vert)

        return vert, horiz

    def total_width(self):
        return self.width + self.padding()[1] * 2

    def total_height(self):
        return self.height + self.padding()[0] * 2


class SpriteFactory():
    cssTemplate = (
        '.{0} {{ background: transparent url("{1}") no-repeat ' +
        ' -{2}px -{3}px scroll; /* height: {4}px; width: {5}px; */ }}'
    )

    def __init__(self, images):
        self.images = images

    def spritify(self, version):
        css = []

        maxw = 0
        totalHeight = 0

        for item in self.images:
            width = item.total_width()

            if maxw < width:
                maxw = width

            totalHeight = totalHeight + item.total_height()

        sprite = Image.new('RGBA', (maxw, totalHeight))

        cursor = 0, 0

        for item in self.images:
            padding = item.padding()
            cursor = (cursor[0] + padding[1], cursor[1] + padding[0])
            sprite.paste(item.image, cursor)

            css.append(
                SpriteFactory.cssTemplate.format(
                    item.classname(),
                    'sprite.png{0}'.format(
                        "" if version is None else "?v={0}".format(version)),
                    cursor[0],
                    cursor[1],
                    item.height,
                    item.width
                )
            )

            cursor = (0, cursor[1] + item.height + padding[0])

        return sprite, css


def fullpaths(dirpath):
    files = sorted(os.listdir(dirpath))
    for file in files:
        yield os.path.join(dirpath, file)


def getArgs():
    parser = argparse.ArgumentParser(
        description='Creates a sprite from a directory of images.'
    )

    parser.add_argument(
        'target',
        type=str,
        help='The target folder containing the images for the sprite.'
    )

    parser.add_argument(
        '--output',
        type=str,
        default='sprite.png',
        help='The output filename for the sprite.'
    )

    parser.add_argument(
        '--version',
        type=str,
        default=None,
        help='A version number to override caching in the browser.'
    )

    args = parser.parse_args()

    if not os.path.isdir(args.target):
        parser.error('The target must be a directory.')

    return args


def main():
    args = getArgs()

    images = [
        SpriteComponent(Image.open(filename), filename)
        for filename in fullpaths(args.target)
    ]

    factory = SpriteFactory(images)

    sprite, css = factory.spritify(args.version)

    sprite.save(args.output, 'PNG')

    for line in css:
        print line

if __name__ == '__main__':
    main()
